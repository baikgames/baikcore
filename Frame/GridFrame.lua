-- Load Module
local Baik = _G.Baik
local CombatHideFrame = Baik.Frame.CombatHideFrame
local GridAction = Baik.Action.GridAction
local Log = Baik.Base.Log
local LineTexture = Baik.Frame.Texture.LineTexture
local SystemAction = Baik.Action.SystemAction
local Table = Baik.Base.Table

-- Create Module
local GridFrame = Baik:NewModule("GridFrame")

-- Private Methods
local function CreateGridFrame()
    local frame = CreateFrame("Frame",
                              "BaikGridFrame",
                              UIParent,
                              "SecureHandlerBaseTemplate")
    frame:SetAllPoints()
    CombatHideFrame:AddAlphaFrame(frame)

    return frame
end

-- Private API
function GridFrame:_SetupLine(index, x0, y0, x1, y1, thickness, color, parent)
    local line = self._lines[index]
    if line == nil then
        local name = string.format("BaikGridFrame_Line_%d", index)
        line = LineTexture.New(name, parent, x0, y0, x1, y1, thickness, color)
        self._lines[index] = line
        return
    end

    line:SetPoints(x0, y0, x1, y1)
    line:SetColor(color)
    line:SetThickness(thickness)
    line:Show()
end

function GridFrame:_SetupHighlight(width, height, size, color)
    local lines = self._lines
    local x_offset = (width / 2.0) % size
    local max_vert_index = 1 + math.floor((width - x_offset) / size)
    local vert_index = math.ceil(max_vert_index / 2)
    lines[vert_index]:SetColor(color)

    local y_offset = (height / 2.0) % size
    local max_horiz_index = 1 + math.floor((height - y_offset) / size)
    local horiz_index = math.ceil(max_horiz_index / 2) + max_vert_index
    lines[horiz_index]:SetColor(color)
end

function GridFrame:_UpdateLines()
    local frame = self._frame
    local lines = self._lines
    Table.ForEach(lines, function(line)
        line:Hide()
    end)

    local thickness = GridAction.Thickness:Get()
    local color = GridAction.Color:Get()
    local width = SystemAction.ScreenWidth()
    local height = SystemAction.ScreenHeight()
    local size = GridAction.Size:Get()
    local index = 1

    -- Create vert line
    local x_offset = (width / 2.0) % size
    for x = x_offset, width, size do
        self:_SetupLine(index, x, 0, x, height, thickness, color, frame)
        index = index + 1
    end

    -- Create horiz line
    local y_offset = (height / 2.0) % size
    for y = y_offset, height, size do
        self:_SetupLine(index, 0, y, width, y, thickness, color, frame)
        index = index + 1
    end

    local highlight_color = GridAction.Highlight:Get()
    self:_SetupHighlight(width, height, size, highlight_color)
end

function GridFrame:_SetEnable(value)
    if value then
        self._frame:Show()
    else
        self._frame:Hide()
    end
end

-- Constants
local _EVENTS = {
    {
        action = GridAction.Enable,
        method = GridFrame._SetEnable
    },
    {
        action = GridAction.Thickness,
        method = GridFrame._UpdateLines,
    },
    {
        action = GridAction.Size,
        method = GridFrame._UpdateLines,
    },
    {
        action = GridAction.Color,
        method = GridFrame._UpdateLines,
    },
    {
        action = GridAction.Highlight,
        method = GridFrame._UpdateLines,
    },
    -- TODO : hook profile reset event
}

function GridFrame:_SetupEvents()
    Table.ForEach(_EVENTS, function(entry)
        local event = entry.action:Event()
        event:Register(self, entry.method)
    end)
end

-- Ace Callbacks
function GridFrame:OnInitialize()
    self._lines = {}
    self._frame = CreateGridFrame()
    self:_SetupEvents()
    Log:i("GridFrame OnInitialize")
end

function GridFrame:OnEnable()
    Log:i("GridFrame OnEnable")
end

function GridFrame:OnDisable()
    Log:i("GridFrame OnDisable")
end

-- Export Module
Baik.Frame.GridFrame = GridFrame
