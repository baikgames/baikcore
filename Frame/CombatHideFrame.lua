-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

-- Create Module
local CombatHideFrame = Baik:NewModule("CombatHideFrame")

-- Private Methods
local function CreateCombatFrame()
    local frame = CreateFrame("Frame",
                              "BaikCombatFrame",
                              UIParent,
                              "SecureHandlerStateTemplate")
    frame:Execute([=[
        hidden_frames = table.new()
        alpha_frames = table.new()
    ]=])
    frame:SetAttribute("_onstate-combat", [=[
        local hide = newstate == "hide"
        for _, frame in pairs(hidden_frames) do
            if hide then
                frame:Hide()
            else
                frame:Show()
            end
        end

        local alpha = hide and 0 or 1
        for _, frame in pairs(alpha_frames) do
            frame:SetAlpha(alpha)
        end
    ]=])
    RegisterStateDriver(frame, "combat", "[combat] hide; show")

    return frame
end

-- Public API
function CombatHideFrame:AddHideFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_hide_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_hide_frame")

        table.insert(hidden_frames, frame)
    ]=])
end

function CombatHideFrame:AddAlphaFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_alpha_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_alpha_frame")

        table.insert(alpha_frames, frame)
    ]=])
end

-- Ace Callbacks
function CombatHideFrame:OnInitialize()
    self._frame = CreateCombatFrame()
    Log:i("CombatHideFrame OnInitialize")
end

function CombatHideFrame:OnEnable()
    Log:i("CombatHideFrame OnEnable")
end

function CombatHideFrame:OnDisable()
    Log:i("CombatHideFrame OnDisable")
end

-- Export Module
Baik.Frame.CombatHideFrame = CombatHideFrame
