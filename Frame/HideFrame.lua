-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

-- Create Module
local HideFrame = Baik:NewModule("HideFrame")

-- Member Variables
local _parent_frames = {}
local _alpha_frames = {}

-- Private Methods
local function CreateHideFrame()
    local frame = CreateFrame("Frame",
                              "BaikCombatFrame",
                              UIParent)
    frame:Hide()

    return frame
end

-- Public API
function HideFrame:Add(frame)
    Assert.Table(frame)

    local parent = frame:GetParent()
    _parent_frames[frame] = parent

    frame:SetParent(self._frame)
    frame.ignoreFramePositionManager = true
end

function HideFrame:AddAlpha(frame)
    Assert.Table(frame)

    _alpha_frames[frame] = frame:GetAlpha()
    frame:SetAlpha(0.0)
end

function HideFrame:Remove(frame)
    Assert.Table(frame)

    local parent = _parent_frames[frame]
    if parent == nil then
        Log:w("Frame not hidden")
        return
    end
    _parent_frames[frame] = nil

    frame:SetParent(parent)
    frame.ignoreFramePositionManager = false
end

function HideFrame:RemoveAlpha(frame)
    Assert.Table(frame)

    local alpha = _alpha_frames[frame]
    if alpha == nil then
        Log:w("Frame alpha not hidden")
        return
    end
    _alpha_frames[frame] = nil

    frame:SetAlpha(alpha)
end

-- Ace Callbacks
function HideFrame:OnInitialize()
    self._frame = CreateHideFrame()
    Log:i("HideFrame OnInitialize")
end

function HideFrame:OnEnable()
    Log:i("HideFrame OnEnable")
end

function HideFrame:OnDisable()
    Log:i("HideFrame OnDisable")
end

-- Export Module
Baik.Frame.HideFrame = HideFrame
