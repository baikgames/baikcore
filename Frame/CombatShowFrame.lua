-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

-- Create Module
local CombatShowFrame = Baik:NewModule("CombatShowFrame")

-- Private Methods
local function CreateCombatFrame()
    local frame = CreateFrame("Frame",
                              "BaikCombatFrame",
                              UIParent,
                              "SecureHandlerStateTemplate")
    frame:Execute([=[
        show_frames = table.new()
        alpha_frames = table.new()
    ]=])
    frame:SetAttribute("_onstate-combat", [=[
        local hide = newstate == "hide"
        for _, frame in pairs(show_frames) do
            if hide then
                frame:Hide()
            else
                frame:Show()
            end
        end

        local alpha = hide and 0 or 1
        for _, frame in pairs(alpha_frames) do
            frame:SetAlpha(alpha)
        end
    ]=])
    RegisterStateDriver(frame, "combat", "[combat] show; hide")

    return frame
end

-- Public API
function CombatShowFrame:AddFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_show_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_show_frame")

        table.insert(show_frames, frame)
    ]=])
    frame:Hide()
end

function CombatShowFrame:AddAlphaFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_alpha_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_alpha_frame")

        table.insert(alpha_frames, frame)
    ]=])
end

function CombatShowFrame:RemoveFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_show_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_show_frame")

        for idx, value in pairs(show_frames) do
            if value == frame then
                table.remove(show_frames, idx)
                return
            end
        end
    ]=])
    frame:Show()
end

function CombatShowFrame:RemoveAlphaFrame(frame)
    Assert.Table(frame)
    Assert.True(frame:IsProtected())

    local combat_frame = self._frame
    combat_frame:SetFrameRef("register_alpha_frame", frame)
    combat_frame:Execute([=[
        local frame = self:GetFrameRef("register_alpha_frame")

        for idx, value in pairs(alpha_frames) do
            if value == frame then
                table.remove(show_frames, idx)
                return
            end
        end
    ]=])
end

-- Ace Callbacks
function CombatShowFrame:OnInitialize()
    self._frame = CreateCombatFrame()
    Log:i("CombatShowFrame OnInitialize")
end

function CombatShowFrame:OnEnable()
    Log:i("CombatShowFrame OnEnable")
end

function CombatShowFrame:OnDisable()
    Log:i("CombatShowFrame OnDisable")
end

-- Export Module
Baik.Frame.CombatShowFrame = CombatShowFrame
