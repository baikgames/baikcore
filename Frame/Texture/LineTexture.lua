-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

-- Create Module
local LineTexture = Class()

-- Private Method
local function CreateTexture(name, parent, color)
    local texture = parent:CreateTexture(name, "BACKGROUND")
    texture:SetColorTexture(unpack(color))

    return texture
end

local function DrawLine(texture, canvasFrame,
                        start_x, start_y,
                        end_x, end_y,
                        line_width, line_factor, rel_point)
    if (not rel_point) then rel_point = "BOTTOMLEFT" end
    if (not line_factor) then line_factor = 1 end
    line_factor = line_factor * .5

    -- Determine dimensions and center point of line
    local dx,dy = end_x - start_x, end_y - start_y
    local cx,cy = (start_x + end_x) / 2, (start_y + end_y) / 2

    -- Normalize direction if necessary
    if (dx < 0) then
        dx,dy = -dx,-dy
    end

    -- Calculate actual length of line
    local line_length = sqrt((dx * dx) + (dy * dy))

    -- Quick escape if it'sin zero length
    if (line_length == 0) then
        texture:SetTexCoord(0,0,0,0,0,0,0,0)
        texture:SetPoint("BOTTOMLEFT", canvasFrame, rel_point, cx,cy)
        texture:SetPoint("TOPRIGHT",   canvasFrame, rel_point, cx,cy)
        return
    end

    -- Sin and Cosine of rotation, and combination (for later)
    local sin, cos = -dy / line_length, dx / line_length
    local sin_cos = sin * cos

    -- Calculate bounding box size and texture coordinates
    local bounding_width, bounding_height, bottom_left_x, bottom_left_y
    local top_left_x, top_left_y, top_right_x, top_right_y, bottom_right_x, bottom_right_y
    if (dy >= 0) then
        bounding_width = ((line_length * cos) - (line_width * sin)) * line_factor
        bounding_height = ((line_width * cos) - (line_length * sin)) * line_factor

        bottom_left_x = (line_width / line_length) * sin_cos
        bottom_left_y = sin * sin
        bottom_right_y = (line_length / line_width) * sin_cos
        bottom_right_x = 1 - bottom_left_y

        top_left_x = bottom_left_y
        top_left_y = 1 - bottom_right_y
        top_right_x = 1 - bottom_left_x
        top_right_y = bottom_right_x
    else
        bounding_width = ((line_length * cos) + (line_width * sin)) * line_factor
        bounding_height = ((line_width * cos) + (line_length * sin)) * line_factor

        bottom_left_x = sin * sin
        bottom_left_y = -(line_length / line_width) * sin_cos
        bottom_right_x = 1 + (line_width / line_length) * sin_cos
        bottom_right_y = bottom_left_x

        top_left_x = 1 - bottom_right_x
        top_left_y = 1 - bottom_left_x
        top_right_y = 1 - bottom_left_y
        top_right_x = top_left_y
    end

    -- Set texture coordinates and anchors
    texture:ClearAllPoints()
    texture:SetTexCoord(top_left_x, top_left_y,
                        bottom_left_x, bottom_left_y,
                        top_right_x, top_right_y,
                        bottom_right_x, bottom_right_y)
    texture:SetPoint("BOTTOMLEFT",
                     canvasFrame,
                     rel_point,
                     cx - bounding_width,
                     cy - bounding_height)
    texture:SetPoint("TOPRIGHT",
                     canvasFrame,
                     rel_point,
                     cx + bounding_width,
                     cy + bounding_height)
end

-- Private API
function LineTexture:_Update()
    DrawLine(self._texture,
             self._parent,
             self._start_x,
             self._start_y,
             self._end_x,
             self._end_y,
             self._thickness)
end

-- Public API
function LineTexture.New(name, parent, start_x, start_y, end_x, end_y, thickness, color)
    -- Check values
    Assert.String(name)
    Assert.Table(parent)
    Assert.Number(start_x)
    Assert.Number(start_y)
    Assert.Number(end_x)
    Assert.Number(end_y)
    Assert.Number(thickness)
    Assert.Table(color)

    -- Create obj
    local obj = Class(LineTexture)
    obj._parent = parent
    obj._texture = CreateTexture(name, parent, color)
    obj._start_x = start_x
    obj._start_y = start_y
    obj._end_x = end_x
    obj._end_y = end_y
    obj._thickness = thickness

    -- Create line
    obj:_Update()

    return obj
end

function LineTexture:SetPoints(start_x, start_y, end_x, end_y)
    Assert.Number(start_x)
    Assert.Number(start_y)
    Assert.Number(end_x)
    Assert.Number(end_y)

    self._start_x = start_x
    self._start_y = start_y
    self._end_x = end_x
    self._end_y = end_y

    self:_Update()
end

function LineTexture:SetThickness(thickness)
    Assert.Number(thickness)

    self._thickness = thickness

    self:_Update()
end

function LineTexture:SetColor(color)
    Assert:Table(color)

    self._texture:SetColorTexture(unpack(color))
end

function LineTexture:Hide()
    self._texture:Hide()
end

function LineTexture:Show()
    self._texture:Show()
end

function LineTexture:Get()
    return self._texture
end

-- Export Module
Baik.Frame.Texture.LineTexture = LineTexture
