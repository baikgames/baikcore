## Interface: 90002
## Version: 9.0.2
## Title: BaikCore
## Author: Nikpmup
## Notes: Core functions for Baik addons
## OptionalDeps: Ace3
## SavedVariables: BaikDB
## X-Embeds: Ace3
Dependencies.xml
BaikCore.lua
Include.xml
