-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

-- Create Module
local CombatSafe = Class()

-- Public API
function CombatSafe:CallbackFunc(hook, ...)
    Assert.Function(hook)

    local args =  { ... }
    return function()
        if InCombatLockdown() then
            return
        end

        hook(unpack(args))
    end
end

function CombatSafe:CallbackMethod(obj, method, ...)
    Assert.Table(obj)
    Assert.Function(method)

    local args =  { ... }
    return function()
        if InCombatLockdown() then
            return
        end

        method(obj, unpack(args))
    end
end

-- Export Module
Baik.Base.CombatSafe = CombatSafe
