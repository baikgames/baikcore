-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

-- Create Module
local Table = Class()

-- Public API
function Table.ContainsValue(dict, value)
    Assert.Table(dict)
    Assert.NotNull(value)

    for _, table_value in pairs(dict) do
        if table_value == value then
            return true
        end
    end

    return false
end

function Table.ForEach(dict, action)
    Assert.Table(dict)
    Assert.Function(action)

    for key, value in pairs(dict) do
        if action(value, key) then
            break
        end
    end
end

function Table.HasKey(dict, key)
    if dict == nil or key == nil then
        return false
    end

    if dict[key] == nil then
        return false
    end

    return true
end

function Table.Print(dict, log)
    Assert.Table(dict)
    for key, value in pairs(dict) do
        Baik:Print("Key: " .. key .. " : " .. value)
    end
end

function Table.RemoveValue(dict, value)
    Assert.Table(dict)
    Assert.NotNull(value)

    for idx = #dict, 1, -1 do
        local table_value = dict[idx]
        if table_value == value then
            return table.remove(dict, idx)
        end
    end

    return nil
end

-- Export Module
Baik.Base.Table = Table
