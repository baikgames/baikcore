-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

-- Create Module
local Math = Class()

-- Public API
function Math.Clamp(value, min, max)
    Assert.Number(value)
    Assert.Number(min)
    Assert.Number(max)

    if value < min then
        return min
    end

    if value > max then
        return max
    end

    return value
end

-- Export
Baik.Base.Math = Math
