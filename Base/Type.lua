-- Load Modules
local Baik = _G.Baik

-- Create Module
local Type = Baik.Base.Class()

-- Private Methods
function _IsType(value, val_type)
    return type(value) == val_type
end

-- Public API
function Type.IsBoolean(value)
    return _IsType(value, "boolean")
end

function Type.IsFunction(value)
    return _IsType(value, "function")
end

function Type.IsNumber(value)
    return _IsType(value, "number")
end

function Type.IsString(value)
    return _IsType(value, "string")
end

function Type.IsTable(value)
    return _IsType(value, "table")
end

function Type.IsChild(value, parent)
    if not Type.IsTable(value) or not Type.IsTable(parent) then
        return false
    end

    if value == parent then
        return true
    end

    while value ~= nil do
        value = getmetatable(value)
        if value == parent then
            return true
        end
    end

    return false
end

-- Export Module
Baik.Base.Type = Type
