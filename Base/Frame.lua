-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

-- Create Module
local Frame = Class()

-- Public API
function Frame.FromPattern(pattern, max_num)
    local frames = {}

    for idx = 1, max_num, 1 do
        local name = string.format(pattern, idx)
        local frame = _G[name]
        Assert.NotNull(frame)

        frames[idx] = frame
    end

    return frames
end

-- Export
Baik.Base.Frame = Frame
