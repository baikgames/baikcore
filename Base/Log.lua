-- Load Module
local Baik = _G.Baik

-- Create Module
local Log = Baik.Base.Class()

-- Constants
local _DEBUG = 3
local _INFO = 2
local _WARN = 1
local _ERROR = 0

-- Member Variables
local _level = _WARN

-- Private Functions
local function _Print(...)
    Baik:Print(...)
end

local function _Printf(format, ...)
    Baik:Printf(format, ...)
end

function Log:d(...)
    if _level < _DEBUG then
        return
    end

    _Print("[DEBUG]", ...)
end

function Log:i(...)
    if _level < _INFO then
        return
    end

    _Print("[INFO]", ...)
end

function Log:w(...)
    if _level < _WARN then
        return
    end

    _Print("[WARN]", ...)
end

function Log:e(...)
    if _level < _ERROR then
        return
    end

    _Print("[ERROR]", ...)
end

-- Export Module
Baik.Base.Log = Log
