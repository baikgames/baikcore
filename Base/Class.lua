-- Load Modules
local Baik = _G.Baik

-- Create Method
local function Class(base_class)
    local new_class = {}
    new_class.__index = new_class

    if type(base_class) == "table" then
        setmetatable(new_class, base_class)
    end

    return new_class
end

-- Export Module
Baik.Base.Class = Class
