-- Load Modules
local Baik = _G.Baik
local Type = Baik.Base.Type

-- Create Module
local Assert = Baik.Base.Class()

-- Private Methods
local function _Assert(value, msg, default_msg)
    msg = msg or default_msg
    assert(value, msg)
end

-- Public API
function Assert.Fail(msg)
    _Assert(false, msg, "Fail")
end

function Assert.False(value, msg)
    _Assert(value == false, msg, "Value is not false")
end

function Assert.True(value, msg)
    _Assert(value == true, msg, "Value is not true")
end

function Assert.Null(value, msg)
    _Assert(value == nil, msg, "Value is not nil")
end

function Assert.NotNull(value, msg)
    _Assert(value ~= nil, msg, "Value is nil")
end

function Assert.Boolean(value, msg)
    _Assert(Type.IsBoolean(value), msg, "Value is not a boolean")
end

function Assert.Function(value, msg)
    _Assert(Type.IsFunction(value), msg, "Value is not a function")
end

function Assert.Number(value, msg)
    _Assert(Type.IsNumber(value), msg, "Value is not a number")
end

function Assert.String(value, msg)
    _Assert(Type.IsString(value), msg, "Value is not a string")
end

function Assert.Table(value, msg)
    _Assert(Type.IsTable(value), msg, "Value is not a table")
end

function Assert.Child(value, parent, msg)
    _Assert(Type.IsChild(value, parent), msg, "Value is not a child")
end

-- Export Module
Baik.Base.Assert = Assert
