-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert

-- Create Module
local Color = Baik.Base.Class()

-- Constants
local _RED_IDX = 1
local _GREEN_IDX = 2
local _BLUE_IDX = 3
local _ALPHA_IDX = 4

-- Private Methods
local function GetColor(colors, idx)
    local color = colors[idx]
    Assert.Number(color)

    return color
end

-- Public API
function Color.New(red, green, blue, alpha)
    local _red = red or 0
    local _green = green or 0
    local _blue = blue or 0
    local _alpha = alpha or 1

    return { _red, _green, _blue, _alpha }
end

function Color.Red(color)
    Assert.Table(color)

    return GetColor(color, _RED_IDX)
end

function Color.Green(color)
    Assert.Table(color)

    return GetColor(color, _GREEN_IDX)
end

function Color.Blue(color)
    Assert.Table(color)

    return GetColor(color, _BLUE_IDX)
end

function Color.Alpha(color)
    Assert.Table(color)

    return GetColor(color, _ALPHA_IDX)
end

function Color.RGB(color)
    Assert.Table(color)

    return { Color.Red(color), Color.Green(color), Color.Blue(color) }
end

-- Export Module
Baik.Base.Color = Color
Color.RED_IDX = _RED_IDX
Color.GREEN_IDX = _GREEN_IDX
Color.BLUE_IDX = _BLUE_IDX
Color.ALPHA_IDX = _ALPHA_IDX
