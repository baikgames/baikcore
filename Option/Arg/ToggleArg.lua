-- Load Module
local Baik = _G.Baik
local ArgBase = Baik.Option.Arg.ArgBase
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local CombatSafe = Baik.Base.CombatSafe
local Log = Baik.Base.Log
local RepositoryAction = Baik.Action.RepositoryAction

-- Create Module
local ToggleArg = Class(ArgBase)

-- Public API
function ToggleArg.New(name, key, action, order, width)
    Assert.String(key)
    Assert.Child(action, RepositoryAction)

    local obj = Class(ToggleArg)
    obj._option = {
        name = name,
        type = "toggle",
        order = order,
        disabled = InCombatLockdown,
        get = function(...)
            return action:Get()
        end,
        set = function(obj, value, ...)
            if InCombatLockdown() then
                Log:w("Cannot change option in combat")
                return
            end

            action:Set(value)
        end,
        width = width
    }
    obj._key = key

    return obj
end

function ToggleArg:Key()
    return self._key
end

function ToggleArg:Get()
    return self._option
end

-- Export Module
Baik.Option.Arg.ToggleArg = ToggleArg
