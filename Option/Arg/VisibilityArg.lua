-- Load Modules
local Baik = _G.Baik
local ArgBase = Baik.Option.Arg.ArgBase
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local RepositoryAction = Baik.Action.RepositoryAction
local SelectArg = Baik.Option.Arg.SelectArg

-- Create Module
local VisibilityArg = Class(ArgBase)

-- Constants
local _VALUES = {
    "Show",
    "Hide",
    "Hover",
    "Combat Only"
}

-- Public API
function VisibilityArg.New(name, key, action, order, width)
    Assert.String(name)
    Assert.String(key)
    Assert.Child(action, RepositoryAction)

    local obj = SelectArg.New(name, key, _VALUES, action, order, width)

    return obj
end

-- Export Module
Baik.Option.Arg.VisibilityArg = VisibilityArg
