-- Load Modules
local Baik = _G.Baik
local ArgBase = Baik.Option.Arg.ArgBase
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local RepositoryAction = Baik.Action.RepositoryAction
local SelectArg = Baik.Option.Arg.SelectArg

local LibSharedMedia = LibStub("LibSharedMedia-3.0")

-- Create Module
local StatusBarArg = Class(ArgBase)

-- Public API
function StatusBarArg.New(name, key, action, order, width)
    Assert.String(name)
    Assert.String(key)
    Assert.Child(action, RepositoryAction)

    local obj = SelectArg.New(name, key, AceGUIWidgetLSMlists.statusbar, action, order, width)
    obj._option["dialogControl"] = "LSM30_Statusbar"

    return obj
end

-- Export Module
Baik.Option.Arg.StatusBarArg = StatusBarArg
