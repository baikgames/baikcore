-- Load Module
local Baik = _G.Baik
local AceDBOptions = LibStub("AceDBOptions-3.0")
local ArgBase = Baik.Option.Arg.ArgBase
local Class = Baik.Base.Class
local Repository = Baik.Data.Repository

-- Create Module
local ProfileArg = Class(ArgBase)

-- Public API
function ProfileArg:Key()
    return "profile"
end

function ProfileArg:Get()
    local database = Repository:Data()
    return AceDBOptions:GetOptionsTable(database)
end

-- Export Module
Baik.Option.Arg.ProfileArg = ProfileArg
