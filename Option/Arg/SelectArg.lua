-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local RepositoryAction = Baik.Action.RepositoryAction
local ArgBase = Baik.Option.Arg.ArgBase

-- Create Module
local SelectArg = Class(ArgBase)

-- Public API
function SelectArg.New(name, key, values, action, order, width)
    Assert.String(name)
    Assert.String(key)
    Assert.Child(action, RepositoryAction)

    local obj = Class(SelectArg)
    obj._key = key
    obj._option = {
        name = name,
        type = "select",
        order = order,
        values = values,
        get = function(...)
            return action:Get()
        end,
        set = function(obj, value, ...)
            if InCombatLockdown() then
                Log:w("Cannot change option in combat")
                return
            end

            action:Set(value)
        end,
        width = width
    }

    return obj
end

function SelectArg:Key()
    return self._key
end

function SelectArg:Get()
    return self._option
end

-- Export Module
Baik.Option.Arg.SelectArg = SelectArg
