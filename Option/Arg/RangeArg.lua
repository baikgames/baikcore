-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local RepositoryAction = Baik.Action.RepositoryAction
local ArgBase = Baik.Option.Arg.ArgBase

-- Create Module
local RangeArg = Class(ArgBase)

-- Public API
function RangeArg.New(name, key, action, min, max, step, order, width)
    Assert.String(name)
    Assert.String(key)
    Assert.Child(action, RepositoryAction)

    local obj = Class(RangeArg)
    obj._key = key
    obj._option = {
        name = name,
        type = "range",
        order = order,
        min = min,
        max = max,
        step = step,
        get = function(...)
            return action:Get()
        end,
        set = function(obj, value, ...)
            if InCombatLockdown() then
                Log:w("Cannot change option in combat")
                return
            end

            action:Set(value)
        end,
        width = width
    }

    return obj
end

function RangeArg:Key()
    return self._key
end

function RangeArg:Get()
    return self._option
end

-- Export Module
Baik.Option.Arg.RangeArg = RangeArg
