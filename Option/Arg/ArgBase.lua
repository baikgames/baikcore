-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local NotImplementedError = Baik.Error.NotImplementedError
local OptionEntry = Baik.Option.OptionEntry

-- Create Module
local ArgBase = Class(OptionBase)

-- Constants
local _WIDTH_NORMAL = "normal"
local _WIDTH_DOUBLE = "double"
local _WIDTH_HALF = "half"
local _WIDTH_FULL = "full"

-- Export Module
Baik.Option.Arg.ArgBase = ArgBase
ArgBase.WIDTH_NORMAL = _WIDTH_NORMAL
ArgBase.WIDTH_DOUBLE = _WIDTH_DOUBLE
ArgBase.WIDTH_HALF = _WIDTH_HALF
ArgBase.WIDTH_FULL = _WIDTH_FULL
