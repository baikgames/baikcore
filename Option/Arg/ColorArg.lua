-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Color = Baik.Base.Color
local Class = Baik.Base.Class
local RepositoryAction = Baik.Action.RepositoryAction
local ArgBase = Baik.Option.Arg.ArgBase

-- Create Module
local ColorArg = Class(ArgBase)

-- Public API
function ColorArg.New(name, key, action, order, width)
    Assert.String(key)
    Assert.String(name)
    Assert.Child(action, RepositoryAction)

    local obj = Class(ColorArg)
    obj._key = key
    obj._option = {
        name = name,
        type = "color",
        order = order,
        disabled = InCombatLockdown,
        get = function(...)
            return unpack(action:Get())
        end,
        set = function(obj, red, green, blue, alpha, ...)
            if InCombatLockdown() then
                Log:w("Cannot change option in combat")
                return
            end

            local color = Color.New(red, green, blue, alpha)
            action:Set(color)
        end,
        hasAlpha = true,
        width = width
    }

    return obj
end

function ColorArg:Key()
    return self._key
end

function ColorArg:Get()
    return self._option
end

-- Export Module
Baik.Option.Arg.ColorArg = ColorArg
