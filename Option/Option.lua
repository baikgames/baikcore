-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local Category = Baik.Option.Category
local Log = Baik.Base.Log
local ProfileArg = Baik.Option.Arg.ProfileArg

-- Create Module
local Option = Baik:NewModule("Option")

-- Constants
local _NAME = "Baik"
local _ARGS = {
    ProfileArg
}

-- Private Variables
local _category = Category.New("Baik", "baik", Category.TREE)

-- Public API
function Option:Category()
    return _category
end

-- Ace Callbacks
function Option:OnInitialize()
    Log:i("Option OnInitialize")
end

function Option:OnEnable()
    local option = _category:Get()
    _category:AddAll(_ARGS)
    AceConfig:RegisterOptionsTable(_NAME, option)
    AceConfigDialog:AddToBlizOptions(_NAME, _NAME)
    Log:i("Option OnEnable")
end

function Option:OnDisable()
    Log:i("Option OnDisable")
end

-- Export Module
Baik.Option.Option = Option
