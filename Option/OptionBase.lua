-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local NotImplementedError = Baik.Error.NotImplementedError

-- Create Module
local OptionBase = Class()

-- Public API
function OptionBase:Key()
    NotImplementedError()
end

function OptionBase:Get()
    NotImplementedError()
end

-- Export Module
Baik.Option.OptionBase = OptionBase
