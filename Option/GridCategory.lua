-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local ColorArg = Baik.Option.Arg.ColorArg
local GridAction = Baik.Action.GridAction
local Option = Baik.Option.Option
local RangeArg = Baik.Option.Arg.RangeArg
local ToggleArg = Baik.Option.Arg.ToggleArg

-- Create Module
local GridCategory = Category.New("Grid", "grid", Category.TREE, 10)

-- Constants
local _ARGS = {
    ToggleArg.New("Enable", "enable", GridAction.Enable, 0, ToggleArg.WIDTH_FULL),
    RangeArg.New("Thickness", "thickness", GridAction.Thickness, 1, 16, 1, 1, ToggleArg.WIDTH_FULL),
    RangeArg.New("Size", "size", GridAction.Size, 2, 256, 2, 2, ToggleArg.WIDTH_FULL),
    ColorArg.New("Color", "color", GridAction.Color, 3, ToggleArg.WIDTH_FULL),
    ColorArg.New("Highlight", "highlight", GridAction.Highlight, 4, ToggleArg.WIDTH_FULL),
}

-- Setup Module
GridCategory:AddAll(_ARGS)
Option:Category():Add(GridCategory)

-- Export Module
Baik.Option.GridCategory = GridCategory
