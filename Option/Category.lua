-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local OptionBase = Baik.Option.OptionBase
local Table = Baik.Base.Table
local NotImplementedError = Baik.Error.NotImplementedError

-- Create Module
local Category = Class(OptionBase)

-- Constants
local _TREE = "tree"
local _TAB = "tab"
local _SELECT = "select"

-- Public API
function Category.New(name, key, group_type, order)
    Assert.String(name)
    Assert.String(key)
    Assert.String(group_type)

    local obj = Class(Category)
    obj._key = key
    obj._option = {
        name = name,
        type = "group",
        childGroups = group_type,
        args = {},
        order = order
    }

    return obj
end

function Category:Add(entry)
    Assert.Table(entry)
    local key = entry:Key()
    local option = entry:Get()

    self._option.args[key] = option
end

function Category:AddAll(args)
    Assert.Table(args)
    Table.ForEach(args, function(arg)
        self:Add(arg)
    end)
end

function Category:Key()
    return self._key
end

function Category:Get()
    return self._option
end

-- Export Module
Baik.Option.Category = Category
Category.TREE = _TREE
Category.TAB = _TAB
Category.SELECT = _SELECT
