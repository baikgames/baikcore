-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event
local Log = Baik.Base.Log
local Repository = Baik.Data.Repository

-- Create Module
local ProfileEvent = Baik:NewModule("ProfileEvent")

-- Public
function ProfileEvent:Get()
    return self._event
end

-- Ace Callbacks
function ProfileEvent:OnInitialize()
    self._event = Event.New()
    Log:i("ProfileEvent OnInitialize")
end

function ProfileEvent:OnEnable()
    local repo = Repository:Data()
    local event = self._event
    repo.RegisterCallback(event,
                          "OnProfileReset",
                          function(...) event:Trigger(...) end)
    Log:i("ProfileEvent OnEnable")
end

function ProfileEvent:OnDisable()
    Log:i("ProfileEvent OnDisable")
end

-- Export Module
Baik.Event.ProfileEvent = ProfileEvent
