-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event

-- Create Module
local GameEvent = Class()

-- Private Methods
local _events = {}

-- Public API
function GameEvent:Get(name)
    Assert.String(name)

    local event = _events[name]
    if event == nil then
        event = Event.New()
        _events[name] = event
        Baik:RegisterEvent(name, function(name, ...)
            event:Trigger(...)
        end)
    end

    return event
end

-- Export Module
Baik.Event.GameEvent = GameEvent
