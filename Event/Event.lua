-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Table = Baik.Base.Table
local Type = Baik.Base.Type

-- Create Module
local Event = Class()

-- Private API
function Event:_RegisterFunction(hook)
    table.insert(self._hooks, hook)
end

function Event:_RegisterObject(obj, method)
    self._obj_hooks[obj] = method
end

function Event:_UnregisterFunction(hook)
    Table.RemoveValue(self._hooks, hook)
end

function Event:_UnregisterObject(hook)
    self._obj_hooks[hook] = nil
end

function Event:_TriggerFunction(...)
    local hooks = self._hooks
    local args = { ... }
    Table.ForEach(hooks, function(hook)
        hook(unpack(args))
    end)
end

function Event:_TriggerObject(...)
    local obj_hooks = self._obj_hooks
    local args = { ... }
    Table.ForEach(obj_hooks, function(method, obj)
        method(obj, unpack(args))
    end)
end

-- Public API
function Event.New()
    local obj = Class(Event)
    obj._hooks = {}
    obj._obj_hooks = {}

    return obj
end

function Event:Register(hook, method)
    Assert.NotNull(hook)

    if Type.IsFunction(hook) then
        self:_RegisterFunction(hook)
        return
    end

    Assert.Table(hook)
    Assert.Function(method)
    self:_RegisterObject(hook, method)
end

function Event:Unregister(hook)
    Assert.NotNull(hook)

    if Type.IsFunction(hook) then
        self:_UnregisterFunction(hook)
        return
    end

    Assert.Table(hook)
    self:_UnregisterObject(hook)
end

function Event:Trigger(...)
    self:_TriggerFunction(...)
    self:_TriggerObject(...)
end

-- Export Module
Baik.Event.Event = Event
