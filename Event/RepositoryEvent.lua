-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event

-- Create Module
local RepositoryEvent = Class()

-- Private Methods
local _events = {}

-- Public API
function RepositoryEvent:Get(key)
    Assert.String(key)

    local event = _events[key]
    if event == nil then
        event = Event.New()
        _events[key] = event
    end

    return event
end

-- Export Module
Baik.Event.RepositoryEvent = RepositoryEvent
