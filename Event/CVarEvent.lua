-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event
local Log = Baik.Base.Log

-- Create Module
local CVarEvent = Baik:NewModule("CVarEvent")

-- Private Methods
local _events = {}

-- Public API
function CVarEvent:Get(name)
    Assert.String(name)

    local event = _events[name]
    if event == nil then
        event = Event.New()
        _events[name] = event
    end

    return event
end

-- Ace Callbacks
function CVarEvent:OnInitialize()
    Log:i("CVarEvent OnInitialize")
end

function CVarEvent:OnEnable()
    Baik:RegisterEvent("CVAR_UPDATE", function(_, cvar, value)
        local event = _events[cvar]
        if event == nil then
            return
        end

        event:Trigger(value)
    end)
    Log:i("CVarEvent OnEnable")
end

function CVarEvent:OnDisable()
    Log:i("CVarEvent OnDisable")
end

-- Export Module
Baik.Event.CVarEvent = CVarEvent
