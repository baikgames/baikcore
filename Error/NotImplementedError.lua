-- Load Modules
local Baik = _G.Baik

-- Create Function
local function NotImplementedError()
    assert(false, "NotImplementedError")
end

-- Export Module
Baik.Error.NotImplementedError = NotImplementedError
