-- Load Module
local Baik = _G.Baik
local AceDB = LibStub("AceDB-3.0")
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

-- Create Module
local Repository = Baik:NewModule("Data")

-- Constants
local _DB = "BaikDB"

-- Member Variables
local _defaults = {
    profile = {}
}

-- Public API
function Repository:Default(key, value)
    Assert.String(key)
    Log:d("Setting: ", key, value)
    _defaults.profile[key] = value
end

function Repository:Get(key)
    Assert.String(key)
    return self._database.profile[key]
end

function Repository:Set(key, value)
    Assert.String(key)
    Log:d("Setting: ", key, value)
    self._database.profile[key] = value
end

function Repository:Data()
    return self._database
end

-- Ace Callbacks
function Repository:OnInitialize()
    Log:i("Repository OnInitialize")
end

function Repository:OnEnable()
    self._database = AceDB:New(_DB, _defaults, true)
    Log:i("Repository OnEnable")
end

function Repository:OnDisable()
    Log:i("Repository OnDisable")
end

-- Export Module
Baik.Data.Repository = Repository
