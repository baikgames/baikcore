-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class

-- Create Module
local Visibility = Class()

-- Public API
Visibility.SHOW = 1
Visibility.HIDE = 2
Visibility.HOVER = 3
Visibility.COMBAT = 4

-- Export Module
Baik.Data.Visibility = Visibility
