-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class

-- Create Module
local PlayerAction = Class()

-- Constants
local _TAG = "player"

-- Public API
function PlayerAction.GetXp()
    return UnitXP(_TAG)
end

function PlayerAction.GetXpMax()
    return UnitXPMax(_TAG)
end

function PlayerAction.GetXpExhaustion()
    return GetXPExhaustion()
end

function PlayerAction:GetLevel()
    return UnitLevel(_TAG)
end

function PlayerAction:GetMaxLevel()
    return GetMaxLevelForPlayerExpansion()
end

function PlayerAction:GetItemLevel()
    return select(2, GetAverageItemLevel())
end

-- Export Module
Baik.Action.PlayerAction = PlayerAction
