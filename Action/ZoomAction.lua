-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log

-- Create Module
local ZoomAction = Class()

-- Constants
local _ZOOM_COORD = { 0.1, 0.9, 0.1, 0.9 }
local _NORM_COORD = { 0.0, 1.0, 0.0, 1.0 }

-- Public API
function ZoomAction.Button(button, zoom)
    Assert.Table(button)

    local icon = button.icon
    if not icon then
        Log:w("Button does not have icon")
        return
    end

    local tex_coord = zoom and _ZOOM_COORD or _NORM_COORD
    icon:SetTexCoord(unpack(tex_coord))
end

-- Export Module
Baik.Action.ZoomAction = ZoomAction
