-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class

local LibSharedMedia = LibStub("LibSharedMedia-3.0")

-- Create Module
local SharedMediaAction = Class()

-- Public API
function SharedMediaAction:DefaultStatusBar()
    return LibSharedMedia:GetDefault(LibSharedMedia.MediaType.STATUSBAR)
end

function SharedMediaAction:StatusBar(name)
    return LibSharedMedia:Fetch(LibSharedMedia.MediaType.STATUSBAR, name)
end

-- Export Module
Baik.Action.SharedMediaAction = SharedMediaAction
