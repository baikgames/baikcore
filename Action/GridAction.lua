-- Load Module
local Baik = _G.Baik
local RepositoryAction = Baik.Action.RepositoryAction
local Color = Baik.Base.Color
local Log = Baik.Base.Log
local ProfileEvent = Baik.Event.ProfileEvent

-- Create Module
local GridAction = Baik:NewModule("GridAction")

GridAction.Enable = RepositoryAction.New("baik_grid_enable", false)
GridAction.Thickness = RepositoryAction.New("baik_grid_thickness", 2)
GridAction.Size = RepositoryAction.New("baik_grid_size", 16)
GridAction.Color = RepositoryAction.New("baik_grid_color", Color.New(0, 0, 0, 0.75))
GridAction.Highlight = RepositoryAction.New("baik_grid_highlight", Color.New(1, 0, 0, 0.75))

-- Private Methods
local function _TriggerEvents()
    GridAction.Enable:TriggerEvent()
    GridAction.Thickness:TriggerEvent()
    GridAction.Size:TriggerEvent()
    GridAction.Color:TriggerEvent()
    GridAction.Highlight:TriggerEvent()
end

-- Ace Callbacks
function GridAction:OnInitialize()
    Log:i("GridAction OnInitialize")
end

function GridAction:OnEnable()
    _TriggerEvents()
    ProfileEvent:Get():Register(_TriggerEvents)
    Log:i("GridAction OnEnable")
end

function GridAction:OnDisable()
    Log:i("GridAction OnDisable")
end

-- Export Module
Baik.Action.GridAction = GridAction
