-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

local LibItemUpgradeInfo = LibStub("LibItemUpgradeInfo-1.0")

-- Create Module
local ItemAction = Class()

-- Private Methods
local function _GetItemInfo(link)
    local id, _, _, _, _, item_type = GetItemInfoInstant(link)

    return id, item_type
end

-- Public API
function ItemAction:InventoryInfo(unit, slot)
    Assert.String(unit)
    Assert.Number(slot)

    -- No item in slot
    local link = GetInventoryItemLink(unit, slot)
    if link == nil then
        return nil, nil, nil
    end

    local id, item_type = _GetItemInfo(link)
    return link, id, item_type
end

function ItemAction:ContainerInfo(bag_id, slot)
    Assert.Number(bag_id)
    Assert.Number(slot)

    -- No item in slot
    local link = GetContainerItemLink(bag_id, slot)
    if link == nil then
        return nil, nil, nil
    end

    local id, item_type = _GetItemInfo(link)
    return link, id, item_type
end

function ItemAction:ItemLevel(link)
    Assert.String(link)

    return LibItemUpgradeInfo:GetUpgradedItemLevel(link)
end

function ItemAction:IsEquipment(item_type)
    return item_type == LE_ITEM_CLASS_WEAPON or item_type == LE_ITEM_CLASS_ARMOR
end

-- Export Module
Baik.Action.ItemAction = ItemAction
