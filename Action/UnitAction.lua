-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Color = Baik.Base.Color

-- Create Module
local UnitAction = Class()

-- Public API
function UnitAction:Class(unit)
    Assert.String(unit)
    return select(2, UnitClass(unit))
end

function UnitAction:ClassColor(unit)
    Assert.String(unit)
    local class = self:Class(unit)
    local color = RAID_CLASS_COLORS[class]
    if color == nil then
        return Color.New(0.3, 0.3, 0.3, 1.0)
    end

    return Color.New(color.r, color.g, color.b, 1.0)
end

function UnitAction:UnitColor(unit)
    Assert.String(unit)

    local r, g, b = UnitSelectionColor(unit)
    return Color.New(r, g, b, 1.0)
end

function UnitAction:IsPlayer(unit)
    Assert.String(unit)

    return UnitIsPlayer(unit)
end

-- Export Module
Baik.Action.UnitAction = UnitAction
