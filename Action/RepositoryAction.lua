-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local Repository = Baik.Data.Repository
local RepositoryEvent = Baik.Event.RepositoryEvent

-- Create Module
local RepositoryAction = Class()

-- Public API
function RepositoryAction.New(key, default)
    Assert.String(key)

    local obj = Class(RepositoryAction)
    obj._key = key
    obj._event = RepositoryEvent:Get(key)

    Repository:Default(key, default)

    return obj
end

function RepositoryAction:Set(value)
    local key = self._key
    local event = self._event
    Log:d("Setting: ", key, value)
    Repository:Set(key, value)
    event:Trigger(value)
end

function RepositoryAction:Get()
    return Repository:Get(self._key)
end

function RepositoryAction:Event()
    return self._event
end

function RepositoryAction:TriggerEvent()
    self:Event():Trigger(self:Get())
end

-- Export Module
Baik.Action.RepositoryAction = RepositoryAction
