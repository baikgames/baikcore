-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local Table = Baik.Base.Table

-- Create Module
local HoverAction = Class()

-- Private Method
local function _SetupHover(root_frame, frame, level)
    level = level - 1
    if level < 0 then
        return
    end

    local children = { frame:GetChildren() }
    Table.ForEach(children, function(child)
        local mouse_enabled = child:IsMouseEnabled()
        child._mouse_enabled = mouse_enabled
        child:EnableMouse(true)

        Baik:SecureHookScript(child, "OnEnter", function()
            root_frame:SetAlpha(1)
        end)
        Baik:SecureHookScript(child, "OnLeave", function()
            root_frame:SetAlpha(0)
        end)
        _SetupHover(root_frame, child, level)
    end)
end

local function _RemoveHover(root_frame, frame, level)
    level = level - 1
    if level < 0 then
        return
    end

    local children = { frame:GetChildren() }
    Table.ForEach(children, function(child)
        frame:EnableMouse(frame._mouse_enabled)
        frame._mouse_enabled = nil
        Baik:Unhook(child, "OnEnter")
        Baik:Unhook(child, "OnLeave")
        _RemoveHover(root_frame, child, level)
    end)
end

-- Public API
function HoverAction.Setup(frame, level)
    Assert.Table(frame)
    Assert.Number(level)

    frame:SetAlpha(0)
    _SetupHover(frame, frame, level)
end

function HoverAction.Remove(frame, level)
    Assert.Table(frame)
    Assert.Number(level)

    frame:SetAlpha(1)
    _RemoveHover(frame, frame, level)
end

-- Export Module
Baik.Action.HoverAction = HoverAction
