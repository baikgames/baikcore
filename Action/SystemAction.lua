-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class

-- Create Module
local SystemAction = Class()

-- Public API
function SystemAction:ScreenWidth()
    return GetScreenWidth()
end

function SystemAction:ScreenHeight()
    return GetScreenHeight()
end

-- Export Module
Baik.Action.SystemAction = SystemAction
