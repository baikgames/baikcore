-- Import Modules
local _G = _G

-- Create Module
local Baik = LibStub("AceAddon-3.0"):NewAddon("Baik",
                                              "AceConsole-3.0",
                                              "AceEvent-3.0",
                                              "AceHook-3.0")

-- Constants
local _NAME = "Baik"

-- Ace Callbacks
function Baik:OnInitialize()
end

function Baik:OnEnable()
end

function Baik:OnDisable()
end

-- Export Module
_G.Baik = Baik
Baik.NAME = _NAME
Baik.DB = _DB
